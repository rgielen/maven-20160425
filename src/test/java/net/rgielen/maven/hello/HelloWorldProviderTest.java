package net.rgielen.maven.hello;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public class HelloWorldProviderTest {

    @Test
    public void testMakeNewGreeting() throws Exception {
        assertEquals("Hello Heinz", new HelloWorldProvider("Heinz").greeting());
    }

}