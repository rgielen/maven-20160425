package net.rgielen.maven.hello;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public class HelloWorldProvider {

    private final String name;

    public HelloWorldProvider(String name) {
        this.name = name;
    }

    public String greeting() {
        return "Hello " + name;
    }
}
