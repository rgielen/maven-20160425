package net.rgielen.maven.hello;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public class HelloWorld {

    public static void main(String[] args) {
        System.out.println(new HelloWorldProvider("Heinz").greeting());
    }
}
